import React, { Component } from "react";
import ReactDOM from "react-dom";
import SideBar from "./general/Sidebar";


export default class Main extends Component {
 
    render() {
        
        return (
            <div>
                <SideBar />
              
            </div>
        );
    }
}

if (document.getElementById("root")) {
    ReactDOM.render(<Main />, document.getElementById("root"));
}
